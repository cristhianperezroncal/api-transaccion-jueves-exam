package com.cperez.transaction.service.impl;

import com.cperez.transaction.dto.TransactionDTO;
import com.cperez.transaction.model.Transaction;
import com.cperez.transaction.repository.TransactionRepository;
import com.cperez.transaction.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {
    @Autowired
    private TransactionRepository transactionRepository;

    @Override
    public List<Transaction> getTransactions() {
        List<Transaction> transactions = transactionRepository.findAll();
        transactions.sort((transaction1,transaction2)-> Double.compare(transaction2.getMonto(), transaction1.getMonto()));
        return transactions;
    }

    @Override
    public Transaction createdTransaction(TransactionDTO transactionDTO) {
        Transaction transaction = new Transaction(transactionDTO);
        return transactionRepository.save(transaction);
    }
}
