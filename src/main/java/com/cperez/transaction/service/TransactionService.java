package com.cperez.transaction.service;

import com.cperez.transaction.dto.TransactionDTO;
import com.cperez.transaction.model.Transaction;

import java.util.List;

public interface TransactionService {
    List<Transaction> getTransactions();
    Transaction createdTransaction(TransactionDTO transactionDTO);
}
