package com.cperez.transaction.controller;

import com.cperez.transaction.dto.TransactionDTO;
import com.cperez.transaction.model.Transaction;
import com.cperez.transaction.service.TransactionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/v1/transactions")
@RequiredArgsConstructor
public class TransactionController {
    @Autowired
    private TransactionService transactionService;

    @GetMapping
    public ResponseEntity<List<Transaction>> finfAll() {
        try {
            List<Transaction> transactions = transactionService.getTransactions();
            return ResponseEntity.status(HttpStatus.OK).body(transactions);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }

    @PostMapping
    public ResponseEntity<Transaction> createdTransaction(@RequestBody TransactionDTO transactionDTO) {
        try {
            Transaction transaction = transactionService.createdTransaction(transactionDTO);
            return ResponseEntity.status(HttpStatus.CREATED).body(transaction);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return ResponseEntity.internalServerError().build();
        }
    }


}
