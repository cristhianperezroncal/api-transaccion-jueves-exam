package com.cperez.transaction.model;

import com.cperez.transaction.dto.TransactionDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
@Data
@Document()
@AllArgsConstructor
@NoArgsConstructor
public class Transaction {
    @Id
    private String id;
    private Client client;
    private String description;
    private double monto;
    private String estado;
    private LocalDateTime date;

    public Transaction(TransactionDTO transactionDTO){
        this.client = transactionDTO.getClient();
        this.description = transactionDTO.getDescription();
        this.monto = transactionDTO.getMonto();
        this.estado = transactionDTO.getEstado();
        this.date = LocalDateTime.now();
    }




}
