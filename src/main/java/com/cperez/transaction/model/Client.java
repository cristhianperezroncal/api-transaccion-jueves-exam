package com.cperez.transaction.model;

import com.cperez.transaction.dto.ClientDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document
public class Client {
    /*@Id
    private String id;*/
    private String name;
    private String phone;
    private String email;
    /*public Client(ClientDTO clientDTO) {
        this.name = clientDTO.getName();
        this.phone = clientDTO.getPhone();
        this.email = clientDTO.getEmail();
    }*/
}
