package com.cperez.transaction.dto;

import com.cperez.transaction.model.Client;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransactionDTO {
    private Client client;
    private String description;
    private double monto;
    private String estado;
    private LocalDateTime date;
}
